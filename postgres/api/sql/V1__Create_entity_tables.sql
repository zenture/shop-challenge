CREATE SEQUENCE hibernate_sequence;

CREATE TABLE customer
(
    customer_id     SERIAL,
    first_name      TEXT NOT NULL,
    last_name       TEXT NOT NULL,
    address1        TEXT NOT NULL,
    address2        TEXT,
    city            TEXT NOT NULL,
    region          TEXT NOT NULL,
    postal_code     TEXT NOT NULL,
    country         TEXT NOT NULL
);

CREATE TABLE product (
    product_id      SERIAL,
    product_name    TEXT NOT NULL,
    sku_id          TEXT NOT NULL,
    description     TEXT NOT NULL,
    image_url       TEXT NOT NULL,
    cost            REAL NOT NULL
);

CREATE TABLE shop_order_cart_products (
  carte_product_id  SERIAL,
  order_order_id    INT,
  product_id        INT NOT NULL,
  count             INT NOT NULL
);

CREATE TABLE shop_order (
    order_id      SERIAL,
    state         TEXT NOT NULL,
    guid          TEXT,
    customer_id   INT,
    product_id    INT,
    product_count INT
);
