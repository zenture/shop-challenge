package com.banders.shop.api.managers;

import com.banders.shop.api.entities.Customer;
import com.banders.shop.api.entities.Order;
import com.banders.shop.api.entities.Product;
import com.banders.shop.api.repositories.CartProductRepository;
import com.banders.shop.api.repositories.CustomerRepository;
import com.banders.shop.api.repositories.OrderRepository;
import com.banders.shop.api.repositories.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

public class OrderManagerTest {

    private OrderManager orderManager;

    @BeforeEach
    void init() {
        orderManager = new OrderManager();
        orderManager.repository = Mockito.mock(OrderRepository.class);
        orderManager.cartProductRepository = Mockito.mock(CartProductRepository.class);
        orderManager.productRepository = Mockito.mock(ProductRepository.class);
        orderManager.customerRepository = Mockito.mock(CustomerRepository.class);
    }

    @Test
    public void createOrderNonExistentCustomer() {
        when(orderManager.customerRepository.findCustomerByCustomerId(anyInt())).thenReturn(Optional.empty());
        assertNull(orderManager.createOrder(-1));
    }

    @Test
    public void createOrderSuccess() {
        when(orderManager.customerRepository.findCustomerByCustomerId(anyInt())).thenReturn(Optional.of(new Customer()));
        when(orderManager.repository.findOrderByGuid(anyString())).thenReturn(Optional.of(new Order()));
        assertNotNull(orderManager.createOrder(-1));
    }

    @Test
    public void addProductToOrderNoOrder() {
        int orderId = -1;
        when(orderManager.repository.findOrderByOrderId(anyInt())).thenReturn(Optional.empty());
        assertEquals("The order " + orderId + " does not exist", orderManager.addProductToOrder(orderId, 1, 1));
    }

    @Test
    public void addProductToOrderNoProduct() {
        int productId = -1;
        when(orderManager.repository.findOrderByOrderId(anyInt())).thenReturn(Optional.of(new Order()));
        when(orderManager.productRepository.findProductByProductId(anyInt())).thenReturn(Optional.empty());
        assertEquals("Product with id " + productId + " does not exist.", orderManager.addProductToOrder(1, productId, 1));
    }

    @Test
    public void addProductToOrderSuccess() {
        int orderId = 1;
        int productId = 1;
        int count = 0;
        when(orderManager.repository.findOrderByOrderId(anyInt())).thenReturn(Optional.of(new Order()));
        when(orderManager.productRepository.findProductByProductId(anyInt())).thenReturn(Optional.of(new Product()));
        assertEquals("Set count of product id " + productId + " to " + count + " for order " +  orderId, orderManager.addProductToOrder(orderId, productId, count));
    }
}
