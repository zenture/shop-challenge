package com.banders.shop.api.repositories;

import com.banders.shop.api.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Optional<Customer> findCustomerByCustomerId(Integer customerId);

    Optional<Customer> findFirstCustomerByFirstNameAndLastNameAndAddress1AndAddress2AndPostalCodeOrderByCustomerIdDesc(String firstName, String lastName, String address1, String address2, String postalCode);
}
