package com.banders.shop.api.repositories;

import com.banders.shop.api.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    Optional<Order> findOrderByOrderId(Integer orderId);

    Optional<Order> findOrderByGuid(String guid);
}
