package com.banders.shop.api.managers;

import com.banders.shop.api.entities.CartProduct;
import com.banders.shop.api.entities.Customer;
import com.banders.shop.api.entities.Order;
import com.banders.shop.api.entities.Product;
import com.banders.shop.api.repositories.CartProductRepository;
import com.banders.shop.api.repositories.CustomerRepository;
import com.banders.shop.api.repositories.OrderRepository;
import com.banders.shop.api.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.*;

@Component
public class OrderManager {

    private final static Logger LOG = LoggerFactory.getLogger(OrderManager.class);

    @Autowired
    OrderRepository repository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CartProductRepository cartProductRepository;

    public Order getOrderByOrderId(Integer orderId) {
        return repository.findOrderByOrderId(orderId).get();
    }

    public Integer createOrder(int customerId) {
        Order order = new Order();
        Optional<Customer> orderCustomer = customerRepository.findCustomerByCustomerId(customerId);
        if(orderCustomer.isPresent()) {
            order.setCustomer(orderCustomer.get());
        } else {
            return null;
        }
        String guid = generateOrderGuid();
        LOG.debug("New order: " + order);
        order.setGuid(guid);
        order.setState("PENDING");
        repository.save(order);
        order = repository.findOrderByGuid(guid).get();
        return order.getOrderId();
    }

    public String addProductToOrder(int orderId, int productId, int count) {
        // check that order exists
        Optional<Order> orderOpt = repository.findOrderByOrderId(orderId);
        if(!orderOpt.isPresent()) {
            LOG.warn(MessageFormat.format("Order {0} does not exist in the database. Cannot add product {1} to it.", orderId, productId));
            return "The order " + orderId + " does not exist";
        }
        Order order = orderOpt.get();
        // check that product exists
        Optional<Product> productOpt = productRepository.findProductByProductId(productId);
        if(!productOpt.isPresent()) {
            LOG.warn(MessageFormat.format("Product {0} does not exist in the database. Cannot add it to order {1} to it.", productId, orderId));
            return "Product with id " + productId + " does not exist.";
        }
        List<CartProduct> orderProducts = order.getCartProducts();
        if(orderProducts == null) {
            orderProducts = new ArrayList<>();
        }
        Product product = productOpt.get();
        // check if product already exists in cart for add or update
        Optional<CartProduct> cartProductOpt = orderProducts.stream().filter(orderProduct -> orderProduct.getProduct().getProductId() == productId).findFirst();
        CartProduct cartProduct;
        cartProduct = cartProductOpt.orElseGet(CartProduct::new);
        cartProduct.setProduct(product);
        cartProduct.setCount(count);
        cartProductRepository.save(cartProduct);
        orderProducts.add(cartProduct);
        order.setCartProducts(orderProducts);
        repository.save(order);

        return "Set count of product id " + productId + " to " + count + " for order " +  orderId;
    }

    public List<Order> getAllOrders() {
        return repository.findAll();
    }

    public List<CartProduct> getAllCartProducts() {
        return cartProductRepository.findAll();
    }

    public boolean deleteOrder(Integer orderId) {
         if(repository.findOrderByOrderId(orderId).isPresent()) {
            repository.deleteById(orderId);
            return true;
         }
         return false;
    }

    /**
     * Generates a guid for an order for use with easy id retrieval after order creation.
     */
    // from https://www.geeksforgeeks.org/generate-random-string-of-given-size-in-java/
    private String generateOrderGuid() {
        int size = 16;
        String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(size);

        for (int i = 0; i < size; i++) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index = (int)(alphaNumericString.length() * Math.random());
            // add Character one by one in end of sb
            sb.append(alphaNumericString.charAt(index));
        }

        return sb.toString();
    }
}
