package com.banders.shop.api.managers;

import com.banders.shop.api.entities.Customer;
import com.banders.shop.api.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerManager {

    @Autowired
    private CustomerRepository repository;

    public Optional<Customer> getCustomerById(Integer customerId) {
        return repository.findCustomerByCustomerId(customerId);
    }

    public List<Customer> getAllCustomers() {
        return repository.findAll();
    }

    public Integer addCustomer(String firstName, String lastName, String address1, String address2, String city, String region, String postalCode, String country) {
        Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setAddress1(address1);
        customer.setAddress2(address2);
        customer.setCity(city);
        customer.setRegion(region);
        customer.setPostalCode(postalCode);
        customer.setCountry(country);
        repository.save(customer);
        return repository.findFirstCustomerByFirstNameAndLastNameAndAddress1AndAddress2AndPostalCodeOrderByCustomerIdDesc(firstName, lastName, address1, address2, postalCode)
                         .get().getCustomerId();
    }

    public boolean deleteCustomer(Integer customerId) {
        if(repository.findCustomerByCustomerId(customerId).isPresent()) {
            repository.deleteById(customerId);
            return true;
        }
        return false;
    }
}
