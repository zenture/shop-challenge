package com.banders.shop.api.controllers;

import com.banders.shop.api.entities.Product;
import com.banders.shop.api.managers.ProductManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductManager manager;

    @RequestMapping(value = "/getProductById", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<Product> getProductsById(@RequestParam(value = "productId") Integer productId) {
        return new ResponseEntity<>(manager.getProductById(productId).get(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllProducts", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<List<Product>> getAllProducts() {
        return new ResponseEntity<>(manager.getAllProducts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addProduct", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    // todo check for skus already being present
    public ResponseEntity<String> addProduct(@RequestParam(value = "productName") String productName,
                                             @RequestParam(value = "skuId") String skuId,
                                             @RequestParam(value = "description") String description,
                                             @RequestParam(value = "imageUrl") String imageUrl,
                                             @RequestParam(value = "cost") Double cost) {
        if(StringUtils.isEmpty(productName)) {
            return new ResponseEntity<>("product_name is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(skuId)) {
            return new ResponseEntity<>("sku_id is a mandatory field", HttpStatus.BAD_REQUEST);
        }  else if(StringUtils.isEmpty(description)) {
            return new ResponseEntity<>("description is a mandatory field", HttpStatus.BAD_REQUEST);
        }  else if(StringUtils.isEmpty(imageUrl)) {
            return new ResponseEntity<>("image_url is a mandatory field", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(manager.addProduct(productName, skuId, description, imageUrl, cost).toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteProduct", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ResponseEntity<String> deleteOrder(@RequestParam(value = "productId") Integer productId) {
        if(manager.deleteProduct(productId)) {
            return new ResponseEntity<>("Deleted product " + productId, HttpStatus.OK);
        }
        return new ResponseEntity<>("Could not delete product " + productId, HttpStatus.BAD_REQUEST);
    }
}
