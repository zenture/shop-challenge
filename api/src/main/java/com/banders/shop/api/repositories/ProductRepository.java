package com.banders.shop.api.repositories;

import com.banders.shop.api.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    Optional<Product> findProductByProductId(Integer productId);

    Optional<Product> findFirstProductBySkuIdOrderByProductIdDesc(String skuId);
}
