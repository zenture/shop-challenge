package com.banders.shop.api.controllers;

import com.banders.shop.api.entities.Customer;
import com.banders.shop.api.managers.CustomerManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerManager manager;

    @RequestMapping(value = "/getCustomerById", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<Customer> getCustomerById(@RequestParam(value = "customerId") Integer customerId) {
        return new ResponseEntity<>(manager.getCustomerById(customerId).get(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllCustomers", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<List<Customer>> getAllCustomers() {
        return new ResponseEntity<>(manager.getAllCustomers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addCustomer", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ResponseEntity<String> addCustomer(@RequestParam(value = "firstName") String firstName,
                                              @RequestParam(value = "lastName") String lastName,
                                              @RequestParam(value = "address1") String address1,
                                              @RequestParam(value = "address2", required = false) String address2,
                                              @RequestParam(value = "city") String city,
                                              @RequestParam(value = "region") String region,
                                              @RequestParam(value = "postalCode") String postalCode,
                                              @RequestParam(value = "country") String country) {
        if(StringUtils.isEmpty(firstName)) {
            return new ResponseEntity<>("first_name is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(lastName)) {
            return new ResponseEntity<>("last_name is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(address1)) {
            return new ResponseEntity<>("address1 is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(city)) {
            return new ResponseEntity<>("image_url is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(region)) {
            return new ResponseEntity<>("region is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(postalCode)) {
            return new ResponseEntity<>("postal_code is a mandatory field", HttpStatus.BAD_REQUEST);
        } else if(StringUtils.isEmpty(country)) {
            return new ResponseEntity<>("image_url is a mandatory field", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(manager.addCustomer(firstName, lastName, address1, address2, city, region, postalCode, country).toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteCustomer", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ResponseEntity<String> deleteOrder(@RequestParam(value = "customerId") Integer customerId) {
        if(manager.deleteCustomer(customerId)) {
            return new ResponseEntity<>("Deleted customer " + customerId, HttpStatus.OK);
        }
        return new ResponseEntity<>("Could not delete customer " + customerId, HttpStatus.BAD_REQUEST);
    }
}
