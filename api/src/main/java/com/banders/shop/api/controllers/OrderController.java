package com.banders.shop.api.controllers;

import com.banders.shop.api.entities.CartProduct;
import com.banders.shop.api.entities.Order;
import com.banders.shop.api.managers.OrderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderManager manager;

    @RequestMapping(value = "/getAllOrders", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<List<Order>> getAllOrders() {
        return new ResponseEntity<>(manager.getAllOrders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllOrderProducts", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<List<CartProduct>> getAllOrderProducts() {
        return new ResponseEntity<>(manager.getAllCartProducts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getOrderById", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<Order> getOrderById(@RequestParam(value = "orderId") Integer orderId) {
        return new ResponseEntity<>(manager.getOrderByOrderId(orderId), HttpStatus.OK);
    }

    @RequestMapping(value = "/createOrder", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ResponseEntity<Integer> createOrder(@RequestParam(value = "customerId") Integer customerId) {
        return new ResponseEntity<>(manager.createOrder(customerId), HttpStatus.OK);
    }

    @RequestMapping(value = "/addProductToOrder", method = {RequestMethod.PUT, RequestMethod.OPTIONS})
    public ResponseEntity<String> addProductToOrder(@RequestParam(value = "orderId") Integer orderId,
                                                    @RequestParam(value = "productId") Integer productId,
                                                    @RequestParam(value = "count") Integer count) {
        return new ResponseEntity<>(manager.addProductToOrder(orderId, productId, count), HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteOrder", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ResponseEntity<String> deleteOrder(@RequestParam(value = "orderId") Integer orderId) {
        if(manager.deleteOrder(orderId)) {
            return new ResponseEntity<>("Deleted order " + orderId, HttpStatus.OK);
        }
        return new ResponseEntity<>("Could not delete order " + orderId, HttpStatus.BAD_REQUEST);
    }
}
