package com.banders.shop.api.managers;

import com.banders.shop.api.entities.Customer;
import com.banders.shop.api.entities.Product;
import com.banders.shop.api.repositories.ProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductManager {

    @Autowired
    private ProductRepository repository;

    public Optional<Product> getProductById(Integer productId) {
        return repository.findProductByProductId(productId);
    }

    public Optional<Product> getProductBySkuId(String skuId) {
        return repository.findFirstProductBySkuIdOrderByProductIdDesc(skuId);
    }

    public List<Product> getAllProducts() {
        return repository.findAll();
    }

    // returns productId
    public Integer addProduct(final String productName, final String skuId, final String description, final String imageUrl, final Double cost) {

        Product product = new Product();
        product.setProductName(productName);
        product.setSkuId(skuId);
        product.setDescription(description);
        product.setImageUrl(imageUrl);
        product.setCost(cost);
        repository.save(product);
        return repository.findFirstProductBySkuIdOrderByProductIdDesc(skuId).get().getProductId();
    }

    public boolean deleteProduct(Integer productId) {
        if(repository.findProductByProductId(productId).isPresent()) {
            repository.deleteById(productId);
            return true;
        }
        return false;
    }
}
