package com.banders.shop.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopApi {

    public static void main(String[] args) {
        SpringApplication.run(ShopApi.class);
    }
}
