package com.banders.shop.api.repositories;


import com.banders.shop.api.entities.CartProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartProductRepository extends JpaRepository<CartProduct, Integer> {
}
