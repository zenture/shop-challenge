package com.banders.shop.client.util;

import com.banders.shop.client.config.ShopClientIntegrationTestConfig;
import com.banders.shop.client.entities.Customer;
import com.banders.shop.client.entities.Order;
import com.banders.shop.client.entities.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ComponentScan(basePackages = "com.banders.shop.client")
public class ShopApiUtilsIntegrationTest {

    @Autowired
    private ShopClientIntegrationTestConfig config;


    @Test
    public void testAddCustomer() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        Integer customerId = utils.addCustomer(config.getCustomer1FirstName(), config.getCustomer1LastName(),
                                               config.getCustomer1Address1(), config.getCustomer1Address2(),
                                               config.getCustomer1City(), config.getCustomer1Region(),
                                               config.getCustomer1PostalCode(), config.getCustomer1Country());
        assertTrue(customerId > 0);
        assertTrue(utils.deleteCustomer(customerId));
    }

    @Test
    public void testGetAllCustomers() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        Integer customerId = utils.addCustomer(config.getCustomer1FirstName(), config.getCustomer1LastName(),
                                               config.getCustomer1Address1(), config.getCustomer1Address2(),
                                               config.getCustomer1City(), config.getCustomer1Region(),
                                               config.getCustomer1PostalCode(), config.getCustomer1Country());
        assertTrue(customerId > 0);
        List<Customer> allCustomers = utils.getAllCustomers();
        assertTrue(allCustomers.size() > 0);
        assertTrue(utils.deleteCustomer(customerId));
    }

    @Test
    public void testGetCustomerById() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        Integer customerId = utils.addCustomer(config.getCustomer1FirstName(), config.getCustomer1LastName(),
                                               config.getCustomer1Address1(), config.getCustomer1Address2(),
                                               config.getCustomer1City(), config.getCustomer1Region(),
                                               config.getCustomer1PostalCode(), config.getCustomer1Country());
        assertTrue(customerId > 0);
        Customer customer = utils.getCustomerById(customerId);
        assertEquals(customer.getCustomerId(), customerId);
        assertTrue(utils.deleteCustomer(customerId));
    }

    @Test
    public void testAddProduct() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        Integer productId = utils.addProduct(config.getProduct1Name(), config.getProduct1SkuId(),
                                             config.getProduct1Description(), config.getProduct1ImageUrl(),
                                             config.getProduct1Cost());
        assertTrue(productId > 0);
        assertTrue(utils.deleteProduct(productId));
    }

    @Test
    public void testGetAllProducts() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        Integer productId = utils.addProduct(config.getProduct1Name(), config.getProduct1SkuId(),
                                             config.getProduct1Description(), config.getProduct1ImageUrl(),
                                             config.getProduct1Cost());
        assertTrue(productId > 0);
        List<Product> allCustomers = utils.getAllProducts();
        assertTrue(allCustomers.size() > 0);
        assertTrue(utils.deleteProduct(productId));
    }

    @Test
    public void testOrder() {
        ShopApiUtils utils = new ShopApiUtils(config.getApiUrl());
        List<Order> initialOrders = utils.getAllOrders();
        Integer customerId = utils.addCustomer(config.getCustomer1FirstName(), config.getCustomer1LastName(),
                                               config.getCustomer1Address1(), config.getCustomer1Address2(),
                                               config.getCustomer1City(), config.getCustomer1Region(),
                                               config.getCustomer1PostalCode(), config.getCustomer1Country());
        assertTrue(customerId > 0);
        Integer productId = utils.addProduct(config.getProduct1Name(), config.getProduct1SkuId(),
                                             config.getProduct1Description(), config.getProduct1ImageUrl(),
                                             config.getProduct1Cost());
        assertTrue(productId > 0);
        Integer orderId = utils.addOrder(customerId);
        assertTrue(orderId > 0);
        int count = 2;
        String addProductResponse = utils.addProductToOrder(orderId, productId, count);
        assertEquals("Set count of product id " + productId + " to " + count + " for order " +  orderId, addProductResponse);

        List<Order> allOrders = utils.getAllOrders();
        assertEquals(initialOrders.size() + 1, allOrders.size());
        assertTrue(utils.deleteOrder(orderId));
        assertTrue(utils.deleteCustomer(customerId));
        assertTrue(utils.deleteProduct(productId));
    }
}