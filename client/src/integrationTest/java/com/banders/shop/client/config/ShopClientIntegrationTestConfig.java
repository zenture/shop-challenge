package com.banders.shop.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application-integration-test.properties")
@Configuration
public class ShopClientIntegrationTestConfig {

    @Value("${test.api.url}")
    private String apiUrl;

    @Value("${product1.name}")
    private String product1Name;

    @Value("${product1.skuId}")
    private String product1SkuId;

    @Value("${product1.description}")
    private String product1Description;

    @Value("${product1.imageUrl}")
    private String product1ImageUrl;

    @Value("${product1.cost}")
    private double product1Cost;

    @Value("${customer1.firstName}")
    private String customer1FirstName;

    @Value("${customer1.lastName}")
    private String customer1LastName;

    @Value("${customer1.address1}")
    private String customer1Address1;

    @Value("${customer1.address2}")
    private String customer1Address2;

    @Value("${customer1.city}")
    private String customer1City;

    @Value("${customer1.region}")
    private String customer1Region;

    @Value("${customer1.postalCode}")
    private String customer1PostalCode;

    @Value("${customer1.country}")
    private String customer1Country;

    public String getApiUrl() {
        return apiUrl;
    }

    public String getProduct1Name() {
        return product1Name;
    }

    public String getProduct1SkuId() {
        return product1SkuId;
    }

    public String getProduct1Description() {
        return product1Description;
    }

    public String getProduct1ImageUrl() {
        return product1ImageUrl;
    }

    public double getProduct1Cost() {
        return product1Cost;
    }

    public String getCustomer1FirstName() {
        return customer1FirstName;
    }

    public String getCustomer1LastName() {
        return customer1LastName;
    }

    public String getCustomer1Address1() {
        return customer1Address1;
    }

    public String getCustomer1Address2() {
        return customer1Address2;
    }

    public String getCustomer1City() {
        return customer1City;
    }

    public String getCustomer1Region() {
        return customer1Region;
    }

    public String getCustomer1PostalCode() {
        return customer1PostalCode;
    }

    public String getCustomer1Country() {
        return customer1Country;
    }
}
