package com.banders.shop.client.managers;

import com.banders.shop.client.config.ShopOrdersConfig;
import com.banders.shop.client.entities.Customer;
import com.banders.shop.client.entities.Order;
import com.banders.shop.client.entities.Product;
import com.banders.shop.client.repositories.CartProductRepository;
import com.banders.shop.client.repositories.CustomerRepository;
import com.banders.shop.client.repositories.OrderRepository;
import com.banders.shop.client.repositories.ProductRepository;
import com.banders.shop.client.util.ShopApiUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

public class ShopClientManagerTest {
    private ShopClientManager manager;

    @BeforeEach
    void init() {
        manager = new ShopClientManager();
        manager.orderRepository = Mockito.mock(OrderRepository.class);
        manager.cartProductRepository = Mockito.mock(CartProductRepository.class);
        manager.productRepository = Mockito.mock(ProductRepository.class);
        manager.customerRepository = Mockito.mock(CustomerRepository.class);
        manager.shopApiUtils = Mockito.mock(ShopApiUtils.class);
        manager.config = new ShopOrdersConfig();
    }

    @Test
    public void fetchEntitiesFromAPISuccess() {
        when(manager.shopApiUtils.getAllCustomers()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllOrders()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllProducts()).thenReturn(new ArrayList<>());
        when(manager.customerRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.productRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.orderRepository.findAll()).thenReturn(new ArrayList<>());
        assertEquals("Fetched entities from the API.", manager.fetchEntitiesFromAPI());

    }

    @Test
    public void fetchEntitiesFromAPICustomersExist() {
        when(manager.shopApiUtils.getAllCustomers()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllOrders()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllProducts()).thenReturn(new ArrayList<>());
        when(manager.customerRepository.findAll()).thenReturn(Collections.singletonList(new Customer()));
        when(manager.productRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.orderRepository.findAll()).thenReturn(new ArrayList<>());
        assertEquals("Database is already populated with data from the API. Delete local data to import again",
                     manager.fetchEntitiesFromAPI());

    }

    @Test
    public void fetchEntitiesFromAPIProductsExist() {
        when(manager.shopApiUtils.getAllCustomers()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllOrders()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllProducts()).thenReturn(new ArrayList<>());
        when(manager.customerRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.productRepository.findAll()).thenReturn(Collections.singletonList(new Product()));
        when(manager.orderRepository.findAll()).thenReturn(new ArrayList<>());
        assertEquals("Database is already populated with data from the API. Delete local data to import again",
                     manager.fetchEntitiesFromAPI());

    }

    @Test
    public void fetchEntitiesFromAPIOrdersExist() {
        when(manager.shopApiUtils.getAllCustomers()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllOrders()).thenReturn(new ArrayList<>());
        when(manager.shopApiUtils.getAllProducts()).thenReturn(new ArrayList<>());
        when(manager.customerRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.productRepository.findAll()).thenReturn(new ArrayList<>());
        when(manager.orderRepository.findAll()).thenReturn(Collections.singletonList(new Order()));
        assertEquals("Database is already populated with data from the API. Delete local data to import again",
                     manager.fetchEntitiesFromAPI());

    }
}
