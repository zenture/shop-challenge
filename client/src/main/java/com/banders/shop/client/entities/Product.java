package com.banders.shop.client.entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

/**
 * A product in the "store." A single product can be
 * associated with many CartProducts.
 */
@Entity
@Table(name = "product")
public class Product {

    @Id
    private int productId;

    @OneToMany(mappedBy = "product")
    private List<CartProduct> cartProducts;

    @NotNull
    private String productName;
    @NotNull
    private String skuId;
    @NotNull
    private String description;
    @NotNull
    private String imageUrl;
    @NotNull
    private double cost;

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", skuId='" + skuId + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", cost=" + cost +
                '}';
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getCost() {
        // courtesy of https://stackoverflow.com/questions/5945867/how-to-round-the-double-value-to-2-decimal-points
        return Math.round(cost*100.0)/100.0;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
