package com.banders.shop.client.controllers;

import com.banders.shop.client.managers.ShopClientManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafController {

    @Autowired
    private ShopClientManager manager;

    @GetMapping("/orders")
    public String orders(Model model) {
        model.addAttribute("orders", manager.getAllOrders());
        return "orders";
    }
}
