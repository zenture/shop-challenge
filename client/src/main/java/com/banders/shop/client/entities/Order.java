package com.banders.shop.client.entities;

import com.google.common.util.concurrent.AtomicDouble;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Orders are the top level object in the api. They hold a collection of
 * CartProducts and have an attached customer.
 */
@Entity
@Table(name = "shop_order")
public class Order {

    @Id
    @Column(name = "order_id")
    private int orderId;

    @NotNull
    private String state;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "order_order_id")
    private List<CartProduct> cartProducts;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")//, referencedColumnName = "customer_id")
    private Customer customer;

    private String guid;

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", state='" + state + '\'' +
                ", cartProducts=" + cartProducts +
                ", customer=" + customer +
                ", guid='" + guid + '\'' +
                '}';
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<CartProduct> getCartProducts() {
        return cartProducts;
    }

    public void setCartProducts(List<CartProduct> orderProducts) {
        this.cartProducts = orderProducts;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTotalOrderPrice() {
        final AtomicDouble orderTotal = new AtomicDouble();
        final NumberFormat decimalFormatter = new DecimalFormat("#0.00");
        cartProducts.forEach(cartProduct -> orderTotal.getAndAdd(cartProduct.getProduct().getCost() * cartProduct.getCount()));
        return "$" + decimalFormatter.format(orderTotal.get());
    }
}
