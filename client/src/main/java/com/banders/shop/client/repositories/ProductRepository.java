package com.banders.shop.client.repositories;

import com.banders.shop.client.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
