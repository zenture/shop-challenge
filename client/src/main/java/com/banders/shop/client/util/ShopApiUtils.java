package com.banders.shop.client.util;

import com.banders.shop.client.entities.CartProduct;
import com.banders.shop.client.entities.Customer;
import com.banders.shop.client.entities.Order;
import com.banders.shop.client.entities.Product;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Methods to interact with the remote API. All flow of data occurs through this class.
 */
public class ShopApiUtils {

    private static Logger LOG = LoggerFactory.getLogger(ShopApiUtils.class);

    public ShopApiUtils(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    private String apiUrl;

    /**
     * Add a new customer to the remote api database
     *
     * @return The new customer_id
     */
    public Integer addCustomer(String firstName, String lastName, String address1, String address2,
                               String city, String region, String postalCode, String country) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("firstName={0}&lastName={1}&address1={2}&address2={3}&city={4}&region={5}&postalCode={6}&country={7}",
                                                  firstName, lastName, address1, address2, city, region, postalCode, country);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/customer/addCustomer?"+queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            return Integer.parseInt(response.getBody());
        }
        return -1;
    }

    public List<Customer> getAllCustomers() {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/customer/getAllCustomers");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request;
        request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.GET, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            final ArrayList<Customer> allCustomers = new ArrayList<>();
            JSONArray allCustomersJson = new JSONArray(response.getBody());
            allCustomersJson.forEach(customerJsonObj -> allCustomers.add(buildCustomerFromJsonObject((JSONObject) customerJsonObj)));
            LOG.debug("All customers json: " + allCustomersJson);
            return allCustomers;
        }
        return new ArrayList<>();
    }

    public Customer getCustomerById(Integer customerId) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("customerId={0}", customerId);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/customer/getCustomerById?" + queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request;
        request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.GET, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            JSONObject customerJson = new JSONObject(response.getBody());
            return buildCustomerFromJsonObject(customerJson);
        }
        return null;
    }

    public boolean deleteCustomer(Integer customerId) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("customerId={0}", customerId);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/customer/deleteCustomer?" + queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request;
        request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        return response.getStatusCode().equals(HttpStatus.OK);
    }

    /**
     * Takes JSONObject from a response body and converts it into a Customer.
     */
    private Customer buildCustomerFromJsonObject(JSONObject customerJson) {
        Customer customer = new Customer();
        customer.setCustomerId((Integer) customerJson.get("customerId"));
        customer.setFirstName((String) customerJson.get("firstName"));
        customer.setLastName((String) customerJson.get("lastName"));
        customer.setAddress1((String) customerJson.get("address1"));
        customer.setAddress2((String) customerJson.get("address2"));
        customer.setCity((String) customerJson.get("city"));
        customer.setRegion((String) customerJson.get("region"));
        customer.setPostalCode((String) customerJson.get("postalCode"));
        customer.setCountry((String) customerJson.get("country"));
        return customer;
    }

    /**
     * Adds a product to the api.
     *
     * @return The new product_id
     */
    public Integer addProduct(String productName, String skuId, String description, String imageUrl, double cost) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("productName={0}&skuId={1}&description={2}&imageUrl={3}&cost={4}", productName, skuId, description, imageUrl, cost);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/product/addProduct?"+queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            return Integer.parseInt(response.getBody());
        }
        return -1;
    }

    public boolean deleteProduct(Integer productId) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("productId={0}", productId);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/product/deleteProduct?"+queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        return response.getStatusCode().equals(HttpStatus.OK);
    }

    public List<Product> getAllProducts() {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/product/getAllProducts");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request;
        request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.GET, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            final ArrayList<Product> allProducts = new ArrayList<>();
            JSONArray allProductsJson = new JSONArray(response.getBody());
            allProductsJson.forEach(productJsonObj -> {
                allProducts.add(buildProductFromJsonObject((JSONObject) productJsonObj));
            });
            LOG.debug("All products json: " + allProductsJson);
            return allProducts;
        }
        return new ArrayList<>();
    }

    /**
     * Takes JSONObject from a response body and converts it into a Product.
     */
    private Product buildProductFromJsonObject(JSONObject productJson) {
        Product product = new Product();
        product.setProductId((Integer) productJson.get("productId"));
        product.setProductName((String) productJson.get("productName"));
        product.setSkuId((String) productJson.get("skuId"));
        product.setDescription((String) productJson.get("description"));
        product.setImageUrl((String) productJson.get("imageUrl"));
        product.setCost(((BigDecimal) productJson.get("cost")).doubleValue());
        return product;
    }

    public Integer addOrder(Integer customerId) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("customerId={0}", customerId);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/order/createOrder?"+queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            return Integer.parseInt(response.getBody());
        }
        return -1;
    }

    /**
     * Adds a count of a product to the specified order.
     *
     * @return The response string from the api endpoint
     */
    public String addProductToOrder(int orderId, int productId, int count) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("orderId={0}&productId={1}&count={2}", orderId, productId, count);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/order/addProductToOrder?"+queryString);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.PUT, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            return response.getBody();
        }
        return null;
    }

    public boolean deleteOrder(Integer orderId) {
        RestTemplate restTemplate = new RestTemplate();
        String queryString = MessageFormat.format("orderId={0}", orderId);
        LOG.debug("Query string: " + queryString);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/order/deleteOrder?"+queryString);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject body = new JSONObject();
        HttpEntity<String> request;
        request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        return response.getStatusCode().equals(HttpStatus.OK);
    }

    public List<Order> getAllOrders() {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl + "/order/getAllOrders");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request;
        request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.GET, request, String.class);
        LOG.debug("Response: body: " + response.getBody() + ", status code: " + response.getStatusCode());
        if(response.getStatusCode().equals(HttpStatus.OK)) {
            final ArrayList<Order> allOrders = new ArrayList<>();
            JSONArray allOrdersJson = new JSONArray(response.getBody());
            allOrdersJson.forEach(productJsonObj -> allOrders.add(buildOrderFromJsonObject((JSONObject) productJsonObj)));
            LOG.debug("All orders json: " + allOrdersJson);
            return allOrders;
        }
        return new ArrayList<>();
    }

    /**
     * Takes JSONObject from a response body and converts it into an order.
     */
    private Order buildOrderFromJsonObject(JSONObject orderJson) {
        Order order = new Order();
        order.setOrderId((Integer) orderJson.get("orderId"));
        order.setState((String) orderJson.get("state"));
        order.setGuid((String) orderJson.get("guid"));
        List<CartProduct> cartProducts = new ArrayList<>();
        JSONArray cartProductsJsonArray = orderJson.getJSONArray("cartProducts");
        cartProductsJsonArray.forEach(cartProductsJsonObj -> cartProducts.add(buildCartProductFromJsonObject((JSONObject) cartProductsJsonObj)));
        order.setCartProducts(cartProducts);
        order.setCustomer(buildCustomerFromJsonObject((JSONObject) orderJson.get("customer")));
        return order;
    }

    /**
     * Takes JSONObject from a response body and converts it into a CartProduct.
     */
    private CartProduct buildCartProductFromJsonObject(JSONObject cartProductJson) {
        CartProduct cartProduct = new CartProduct();
        cartProduct.setCartProductId((Integer) cartProductJson.get("cartProductId"));
        cartProduct.setProduct(buildProductFromJsonObject((JSONObject) cartProductJson.get("product")));
        cartProduct.setCount((Integer) cartProductJson.get("count"));
        return cartProduct;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
}
