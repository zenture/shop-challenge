package com.banders.shop.client.managers;

import com.banders.shop.client.config.ShopOrdersConfig;
import com.banders.shop.client.entities.Order;
import com.banders.shop.client.repositories.CartProductRepository;
import com.banders.shop.client.repositories.CustomerRepository;
import com.banders.shop.client.repositories.OrderRepository;
import com.banders.shop.client.repositories.ProductRepository;
import com.banders.shop.client.util.ShopApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ShopClientManager {

    private static final Logger LOG = LoggerFactory.getLogger(ShopClientManager.class);
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CartProductRepository cartProductRepository;

    @Autowired
    ShopOrdersConfig config;


    ShopApiUtils shopApiUtils;

    @PostConstruct
    public void init() {
        shopApiUtils = new ShopApiUtils(config.getApiUrl());
    }

    /**
     * Creates 4 customers, 4 products and 16 orders by calling the shop API.
     */
    public void populateApiDatabase() {

        // Create customers
        LOG.debug("Populating remote API customers");
        Integer customerId1 = shopApiUtils.addCustomer(config.getCustomer1FirstName(), config.getCustomer1LastName(),
                                                       config.getCustomer1Address1(), config.getCustomer1Address2(),
                                                       config.getCustomer1City(), config.getCustomer1Region(),
                                                       config.getCustomer1PostalCode(), config.getCustomer1Country());
        Integer customerId2 = shopApiUtils.addCustomer(config.getCustomer2FirstName(), config.getCustomer2LastName(),
                                                       config.getCustomer2Address1(), config.getCustomer2Address2(),
                                                       config.getCustomer2City(), config.getCustomer2Region(),
                                                       config.getCustomer2PostalCode(), config.getCustomer2Country());
        Integer customerId3 = shopApiUtils.addCustomer(config.getCustomer3FirstName(), config.getCustomer3LastName(),
                                                       config.getCustomer3Address1(), config.getCustomer3Address2(),
                                                       config.getCustomer3City(), config.getCustomer3Region(),
                                                       config.getCustomer3PostalCode(), config.getCustomer3Country());
        Integer customerId4 = shopApiUtils.addCustomer(config.getCustomer4FirstName(), config.getCustomer4LastName(),
                                                       config.getCustomer4Address1(), config.getCustomer4Address2(),
                                                       config.getCustomer4City(), config.getCustomer4Region(),
                                                       config.getCustomer4PostalCode(), config.getCustomer4Country());
        // Add products
        LOG.debug("Populating remote API products");
        Integer productId1 = shopApiUtils.addProduct(config.getProduct1Name(), config.getProduct1SkuId(),
                                                     config.getProduct1Description(), config.getProduct1ImageUrl(),
                                                     config.getProduct1Cost());
        Integer productId2 = shopApiUtils.addProduct(config.getProduct2Name(), config.getProduct2SkuId(),
                                                     config.getProduct2Description(), config.getProduct2ImageUrl(),
                                                     config.getProduct2Cost());
        Integer productId3 = shopApiUtils.addProduct(config.getProduct3Name(), config.getProduct3SkuId(),
                                                     config.getProduct3Description(), config.getProduct3ImageUrl(),
                                                     config.getProduct3Cost());
        Integer productId4 = shopApiUtils.addProduct(config.getProduct4Name(), config.getProduct4SkuId(),
                                                     config.getProduct4Description(), config.getProduct4ImageUrl(),
                                                     config.getProduct4Cost());

        // Create Orders
        LOG.debug("Creating remote API orders");
        Integer orderId1 = shopApiUtils.addOrder(customerId1);
        Integer orderId2 = shopApiUtils.addOrder(customerId1);
        Integer orderId3 = shopApiUtils.addOrder(customerId1);
        Integer orderId4 = shopApiUtils.addOrder(customerId1);
        Integer orderId5 = shopApiUtils.addOrder(customerId2);
        Integer orderId6 = shopApiUtils.addOrder(customerId2);
        Integer orderId7 = shopApiUtils.addOrder(customerId2);
        Integer orderId8 = shopApiUtils.addOrder(customerId2);
        Integer orderId9 = shopApiUtils.addOrder(customerId3);
        Integer orderId10 = shopApiUtils.addOrder(customerId3);
        Integer orderId11 = shopApiUtils.addOrder(customerId3);
        Integer orderId12 = shopApiUtils.addOrder(customerId3);
        Integer orderId13 = shopApiUtils.addOrder(customerId4);
        Integer orderId14 = shopApiUtils.addOrder(customerId4);
        Integer orderId15 = shopApiUtils.addOrder(customerId4);
        Integer orderId16 = shopApiUtils.addOrder(customerId4);

        // Add products to orders
        LOG.debug("Adding products to remote API orders");
        shopApiUtils.addProductToOrder(orderId1, productId1, 2);
        shopApiUtils.addProductToOrder(orderId2, productId1, 4);
        shopApiUtils.addProductToOrder(orderId3, productId1, 6);
        shopApiUtils.addProductToOrder(orderId4, productId1, 1);
        shopApiUtils.addProductToOrder(orderId5, productId1, 1);
        shopApiUtils.addProductToOrder(orderId5, productId2, 2);
        shopApiUtils.addProductToOrder(orderId6, productId2, 1);
        shopApiUtils.addProductToOrder(orderId6, productId3, 2);
        shopApiUtils.addProductToOrder(orderId7, productId3, 1);
        shopApiUtils.addProductToOrder(orderId7, productId4, 2);
        shopApiUtils.addProductToOrder(orderId8, productId1, 1);
        shopApiUtils.addProductToOrder(orderId8, productId4, 2);
        shopApiUtils.addProductToOrder(orderId9, productId3, 3);
        shopApiUtils.addProductToOrder(orderId9, productId4, 2);
        shopApiUtils.addProductToOrder(orderId10, productId3, 1);
        shopApiUtils.addProductToOrder(orderId10, productId4, 4);
        shopApiUtils.addProductToOrder(orderId11, productId2, 3);
        shopApiUtils.addProductToOrder(orderId11, productId4, 2);
        shopApiUtils.addProductToOrder(orderId12, productId1, 1);
        shopApiUtils.addProductToOrder(orderId12, productId3, 1);
        shopApiUtils.addProductToOrder(orderId13, productId4, 2);
        shopApiUtils.addProductToOrder(orderId14, productId3, 1);
        shopApiUtils.addProductToOrder(orderId15, productId2, 5);
        shopApiUtils.addProductToOrder(orderId16, productId1, 2);
        shopApiUtils.addProductToOrder(orderId16, productId2, 2);
        shopApiUtils.addProductToOrder(orderId16, productId3, 2);
        shopApiUtils.addProductToOrder(orderId16, productId4, 2);
    }


    /**
     * Fetches entities from the remote API and writes them to the database.
     *
     * Entities will not be fetched when the database isn't empty.
     *
     * return The status of the fetch.
     */
    public String fetchEntitiesFromAPI() {

        // don't import if there are already entities in the database
        if(orderRepository.findAll().size() > 0 || customerRepository.findAll().size() > 0 || productRepository.findAll().size() > 0) {
            String message = "Database is already populated with data from the API. Delete local data to import again";
            LOG.info(message);
            return message;
        }
        customerRepository.saveAll(shopApiUtils.getAllCustomers());
        productRepository.saveAll(shopApiUtils.getAllProducts());
        List<Order> allOrders = shopApiUtils.getAllOrders();
        allOrders.forEach(order -> cartProductRepository.saveAll(order.getCartProducts()));
        orderRepository.saveAll(allOrders);
        return "Fetched entities from the API.";
    }

    /**
     * Deletes all entities in the client database.
     */
    public void deleteLocalEntities() {
        LOG.info("Deleting local entities.");
        orderRepository.deleteAll();
        cartProductRepository.deleteAll();
        productRepository.deleteAll();
        customerRepository.deleteAll();
    }

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
