package com.banders.shop.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopClient {

    public static void main(String[] args) {
        SpringApplication.run(ShopClient.class);
    }
}
