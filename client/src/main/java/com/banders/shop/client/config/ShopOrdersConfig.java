package com.banders.shop.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ShopOrdersConfig {

    @Value("${api.url}")
    private String apiUrl;

    @Value("${product1.name}")
    private String product1Name;

    @Value("${product1.skuId}")
    private String product1SkuId;

    @Value("${product1.description}")
    private String product1Description;

    @Value("${product1.imageUrl}")
    private String product1ImageUrl;

    @Value("${product1.cost}")
    private double product1Cost;

    @Value("${product2.name}")
    private String product2Name;

    @Value("${product2.skuId}")
    private String product2SkuId;

    @Value("${product2.description}")
    private String product2Description;

    @Value("${product2.imageUrl}")
    private String product2ImageUrl;

    @Value("${product2.cost}")
    private double product2Cost;

    @Value("${product3.name}")
    private String product3Name;

    @Value("${product3.skuId}")
    private String product3SkuId;

    @Value("${product3.description}")
    private String product3Description;

    @Value("${product3.imageUrl}")
    private String product3ImageUrl;

    @Value("${product3.cost}")
    private double product3Cost;

    @Value("${product4.name}")
    private String product4Name;

    @Value("${product4.skuId}")
    private String product4SkuId;

    @Value("${product4.description}")
    private String product4Description;

    @Value("${product4.imageUrl}")
    private String product4ImageUrl;

    @Value("${product4.cost}")
    private double product4Cost;

    @Value("${customer1.firstName}")
    private String customer1FirstName;

    @Value("${customer1.lastName}")
    private String customer1LastName;

    @Value("${customer1.address1}")
    private String customer1Address1;

    @Value("${customer1.address2}")
    private String customer1Address2;

    @Value("${customer1.city}")
    private String customer1City;

    @Value("${customer1.region}")
    private String customer1Region;

    @Value("${customer1.postalCode}")
    private String customer1PostalCode;

    @Value("${customer1.country}")
    private String customer1Country;

    @Value("${customer2.firstName}")
    private String customer2FirstName;

    @Value("${customer2.lastName}")
    private String customer2LastName;

    @Value("${customer2.address1}")
    private String customer2Address1;

    @Value("${customer2.address2}")
    private String customer2Address2;

    @Value("${customer2.city}")
    private String customer2City;

    @Value("${customer2.region}")
    private String customer2Region;

    @Value("${customer2.postalCode}")
    private String customer2PostalCode;

    @Value("${customer2.country}")
    private String customer2Country;

    @Value("${customer3.firstName}")
    private String customer3FirstName;

    @Value("${customer3.lastName}")
    private String customer3LastName;

    @Value("${customer3.address1}")
    private String customer3Address1;

    @Value("${customer3.address2}")
    private String customer3Address2;

    @Value("${customer3.city}")
    private String customer3City;

    @Value("${customer3.region}")
    private String customer3Region;

    @Value("${customer3.postalCode}")
    private String customer3PostalCode;

    @Value("${customer3.country}")
    private String customer3Country;

    @Value("${customer4.firstName}")
    private String customer4FirstName;

    @Value("${customer4.lastName}")
    private String customer4LastName;

    @Value("${customer4.address1}")
    private String customer4Address1;

    @Value("${customer4.address2}")
    private String customer4Address2;

    @Value("${customer4.city}")
    private String customer4City;

    @Value("${customer4.region}")
    private String customer4Region;

    @Value("${customer4.postalCode}")
    private String customer4PostalCode;

    @Value("${customer4.country}")
    private String customer4Country;

    public String getApiUrl() {
        return apiUrl;
    }

    public String getProduct1Name() {
        return product1Name;
    }

    public String getProduct1SkuId() {
        return product1SkuId;
    }

    public String getProduct1Description() {
        return product1Description;
    }

    public String getProduct1ImageUrl() {
        return product1ImageUrl;
    }

    public double getProduct1Cost() {
        return product1Cost;
    }

    public String getProduct2Name() {
        return product2Name;
    }

    public String getProduct2SkuId() {
        return product2SkuId;
    }

    public String getProduct2Description() {
        return product2Description;
    }

    public String getProduct2ImageUrl() {
        return product2ImageUrl;
    }

    public double getProduct2Cost() {
        return product2Cost;
    }

    public String getProduct3Name() {
        return product3Name;
    }

    public String getProduct3SkuId() {
        return product3SkuId;
    }

    public String getProduct3Description() {
        return product3Description;
    }

    public String getProduct3ImageUrl() {
        return product3ImageUrl;
    }

    public double getProduct3Cost() {
        return product3Cost;
    }

    public String getProduct4Name() {
        return product4Name;
    }

    public String getProduct4SkuId() {
        return product4SkuId;
    }

    public String getProduct4Description() {
        return product4Description;
    }

    public String getProduct4ImageUrl() {
        return product4ImageUrl;
    }

    public double getProduct4Cost() {
        return product4Cost;
    }

    public String getCustomer1FirstName() {
        return customer1FirstName;
    }

    public String getCustomer1LastName() {
        return customer1LastName;
    }

    public String getCustomer1Address1() {
        return customer1Address1;
    }

    public String getCustomer1Address2() {
        return customer1Address2;
    }

    public String getCustomer1City() {
        return customer1City;
    }

    public String getCustomer1Region() {
        return customer1Region;
    }

    public String getCustomer1PostalCode() {
        return customer1PostalCode;
    }

    public String getCustomer1Country() {
        return customer1Country;
    }

    public String getCustomer2FirstName() {
        return customer2FirstName;
    }

    public String getCustomer2LastName() {
        return customer2LastName;
    }

    public String getCustomer2Address1() {
        return customer2Address1;
    }

    public String getCustomer2Address2() {
        return customer2Address2;
    }

    public String getCustomer2City() {
        return customer2City;
    }

    public String getCustomer2Region() {
        return customer2Region;
    }

    public String getCustomer2PostalCode() {
        return customer2PostalCode;
    }

    public String getCustomer2Country() {
        return customer2Country;
    }

    public String getCustomer3FirstName() {
        return customer3FirstName;
    }

    public String getCustomer3LastName() {
        return customer3LastName;
    }

    public String getCustomer3Address1() {
        return customer3Address1;
    }

    public String getCustomer3Address2() {
        return customer3Address2;
    }

    public String getCustomer3City() {
        return customer3City;
    }

    public String getCustomer3Region() {
        return customer3Region;
    }

    public String getCustomer3PostalCode() {
        return customer3PostalCode;
    }

    public String getCustomer3Country() {
        return customer3Country;
    }

    public String getCustomer4FirstName() {
        return customer4FirstName;
    }

    public String getCustomer4LastName() {
        return customer4LastName;
    }

    public String getCustomer4Address1() {
        return customer4Address1;
    }

    public String getCustomer4Address2() {
        return customer4Address2;
    }

    public String getCustomer4City() {
        return customer4City;
    }

    public String getCustomer4Region() {
        return customer4Region;
    }

    public String getCustomer4PostalCode() {
        return customer4PostalCode;
    }

    public String getCustomer4Country() {
        return customer4Country;
    }
}