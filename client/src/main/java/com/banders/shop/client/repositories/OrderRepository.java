package com.banders.shop.client.repositories;

import com.banders.shop.client.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
