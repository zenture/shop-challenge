package com.banders.shop.client.controllers;

import com.banders.shop.client.managers.ShopClientManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shop")
public class ShopClientController {

    @Autowired
    private ShopClientManager manager;

    @RequestMapping(value = "/populateApiDatabase", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<String> populateApiDatabase() {
        manager.populateApiDatabase();
        return new ResponseEntity<>("Populated API database.", HttpStatus.OK);
    }

    @RequestMapping(value = "/fetchApiEntities", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<String> fetchApiEntities() {
        return new ResponseEntity<>(manager.fetchEntitiesFromAPI(), HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteLocalEntities", method = {RequestMethod.GET, RequestMethod.OPTIONS})
    public ResponseEntity<String> deleteLocalEntities() {
        manager.deleteLocalEntities();
        return new ResponseEntity<>("Deleted local entities.", HttpStatus.OK);
    }
}
