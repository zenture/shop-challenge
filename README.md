# Shop Challenge

The repository is composed of four main parts:

1. API: A Spring Boot server which accepts requests to create and retrieve Customer, Product, and Order objects
2. Client: Another Spring boot server which interacts with the API. It populates the API's database with Customers, Products, and Orders, then fetches and store them in its own database for display on an orders page. An endpoint also exists to wipe out the client's local database.
3. Postgres SQL: Contains the SQL to run the databases the API and Client talk to. These schemas are nearly identical, but there are no sequences setting table ids on the client side.
4. Kubernetes configs that run processes from the previous 3 sections.

## Dockerized

The API, Client, and each of the postgres schema definitions have an associated Dockerfile. Images built by these files are used in the docker-compose.yml file at the root of the repository. It starts the postgres instances, checks that they're healthy, then launches the API and Client.

| Process         | External Port | Internal Port |
|-----------------|---------------|---------------|
| API             | 8081          | 8081          |
| API Postgres    | 5433          | 5432          |
| Client          | 8080          | 8080          |
| Client Postgres | 5432          | 5432          |


### Persistence

The compose file defines a volume for each postgres container. This is handy for continued use, but can be frustrating for testing. Commenting out the volumes will cause postgres to be a fresh database with each fresh run of docker-compose which is handy for repeatedly running through the flow.

### Usage 

```bash
cd <root project directory>
docker-compose up
```

Once the stack finishes starting the database will need to be initialized. 

```bash
curl http://localhost:8080/shop/populateApiDatabase
```

Then open the orders page (localhost:8080/orders) in a browser. It will display a mostly blank page with a couple of buttons and no orders.

To populate the orders click the "Fetch Orders" button. This will take you to another page. Navigate back to the order page and refresh it to see the orders.

The delete the Client's orders click the 'Delete Orders' button. This will also take you to another page. Navigate back and refresh here as well to see that the orders are gone.

This process is repeatable.

To stop:

```bash
docker-compose down
docker-compose rm
```

## CI/CD

The .gitlab-ci.yml file at the root of the repository defines 3 stages of jobs: test, build and deploy.

The test phase runs unit tests on the servers. There are a few unit tests for both the Client and API code bases. The Client also has some integration tests in ShopApiUtilsIntegrationTest, but as they require a running API server, they are not automatically executed during CI.

The build phase is composed of 4 jobs: one for each of the Spring Boot servers and 1 for each of their postgres databases. These jobs build a docker image for each process tagged with my Docker id (bryananders) and an image name. These jobs are only run if files have changed in their respective project directories.

| Process         | Image Name                       |
|-----------------|----------------------------------|
| API             | bryananders/shop-api             |
| API Postgres    | bryananders/shop-api-postgres    |
| Client          | bryananders/shop-client          |
| Client Postgres | bryananders/shop-client-postgres |

Like the build phase, the deploy phase has 4 jobs. Each job is responsible for deploying a single Docker image to Docker Hub. Also like the build jobs, these only run if files have changed in their respective paths.

## Kubernetes 

The k8s directory at the top level of the repository contains the configuration files needed to create all of kubernetes objects to run a shop stack like that in docker-compose.yml with a few exceptions. Since this is a small demo kubernetes, the objects are designed to run on a single, physical host, with postgres volumes saving to specified files on disk.

A postgres secret needs to be added:
```bash
kubectl create secret generic pgpassword --from-literal PGPASSWORD=password
```

Create postgres data host directories:

```bash
mkdir -p /data/volumes/shop-api-postgres-persistent-volume-claim
mkdir -p /data/volumes/shop-client-postgres-persistent-volume-claim
```

Create Dashboard Namespace:

```bash
kubectl create namespace 
```

Apply the files:

```bash
kubectl apply -f k8s
```

### Hosts

| Process   | Url                 |
|-----------|---------------------|
| Api       | shop-api.k8s.com    |
| Client    | shop-client.k8s.com |

Ingress services only expose HTTP(S) ports, so all processes listen on port 80.

Ingress hosts need to be added to the hosts file:

```bash
127.0.1.1 <hostname> shop-api.k8s.com shop-client.k8s.com
```


## Known Issues

1. Gradle needs to be using Java 11. Using Java 8 will result in a failure to find plugins. If this issue crops up resolve it by specifying the Java JDK on the command line. Inside the project directory Intellij should handle this automatically if the SDK version is set to 11.

```bash
gradle <task> -Dorg.gradle.java.home=/path/to/java/11/jdk
```

2. The buttons on the orders page don't properly redirect back. Instead you have to navigate back to the orders page and refresh.

3. The status check for the postgres containers in docker-compose spams the message 'UTC [58] FATAL:  role "root" does not exist'. This does not impact functionality.